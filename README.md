# DesignedVR RoiD Assignment

**Hi\
In this project I've focused on building a real "big" application infrastructure,
and not just hard coding components to display in the UI.**

**Have Fun** 😎

### Running instructions with Docker Compose
* `git clone https://roidayan1@bitbucket.org/roidayan1/designedvr.git designedvr_roid`
* `cd designedvr_roid`
* `docker-compose up`
* wait ±400 seconds until the build will finish, and the app will start: `INFO:  Main - Server running on port: 3005`
* go to: http://localhost:3005/


### Running instructions for local development
* `git clone https://roidayan1@bitbucket.org/roidayan1/designedvr.git designedvr_roid`
* `cd designedvr_roid`
* `yarn bootstrap`
* make sure you have a running MySql service on your local machine
* edit the file at `designedvr/packages/server/ormconfig.json` with your mysql username and password
* from designedvr_roid root:
  * console 1: `yarn start-server`
  * console 2: `yarn start-app`
* go to: http://localhost:8005/


## Database MySql
##### commands
* `typeorm migration:run`
* `typeorm migration:create -n MigrationName`

##### column types
`bit`, `int`, `integer`, `tinyint`, `smallint`, `mediumint`, `bigint`, `float`, `double`,
`double precision`, `dec`, `decimal`, `numeric`, `fixed`, `bool`, `boolean`, `date`, `datetime`,
`timestamp`, `time`, `year`, `char`, `nchar`, `national char`, `varchar`, `nvarchar`, `national varchar`,
`text`, `tinytext`, `mediumtext`, `blob`, `longtext`, `tinyblob`, `mediumblob`, `longblob`, `enum`, `set`,
`json`, `binary`, `varbinary`, `geometry`, `point`, `linestring`, `polygon`, `multipoint`, `multilinestring`,
`multipolygon`, `geometrycollection`
