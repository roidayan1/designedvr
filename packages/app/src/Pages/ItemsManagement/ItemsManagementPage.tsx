import React, { ReactNode } from 'react';
import BasePage from '../BasePage';
import styles from './ItemsManagementPage.module.scss';
import Column from '../../BaseComponents/Column/Column';
import Resizer, { ResizerDirection } from '../../BaseComponents/Resizer/Resizer';
import ItemsProvider from '../../Providers/Items/ItemsProvider';
import { Container } from '@material-ui/core';
import AppHeaderConnector from '../../Connectors/AppHeaderConnector/AppHeaderConnector';
import GridHeaderConnector from '../../Connectors/GridHeaderConnector/GridHeaderConnector';
import ItemsGridConnector from '../../Connectors/ItemsGridConnector/ItemsGridConnector';

export type ItemsManagementPageProps = {};

class ItemsManagementPageComponent extends BasePage<ItemsManagementPageProps> {
    static defaultProps: Partial<ItemsManagementPageProps> = {};

    async onLoad() {
        ItemsProvider.fetchSetGetItems().then();
    }

    render(): ReactNode {
        return (
            <div className={styles.container}>
                <AppHeaderConnector />
                <Resizer direction={ResizerDirection.ROW} />
                <Column className={styles.content}>
                    <Container>
                        <GridHeaderConnector />
                    </Container>
                    <div className={styles.grid}>
                        <Container>
                            <ItemsGridConnector />
                        </Container>
                    </div>
                </Column>
            </div>
        );
    }
}

const ItemsManagementPage = ItemsManagementPageComponent;
export default ItemsManagementPage;
