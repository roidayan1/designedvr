import React from 'react';
import BaseConnector, { connectState } from '../BaseConnector';
import BaseStore from '../../Services/StateService';
import HomeMenu from '@dvr/components/src/HomeMenu/HomeMenu';
import { AccordionSection, HomeMenuProps } from '@dvr/components/src/HomeMenu/HomeMenu.model';
import NavigationService, { Routes } from '../../Services/NavigationService';
import OurLeadership from '@dvr/components/src/OurLeadership/OurLeadership';

const stores: Array<BaseStore<any>> = [];

export type HomeMenuConnectorProps = {};

class HomeMenuConnectorComponent extends BaseConnector<HomeMenuConnectorProps, HomeMenuProps> {
    readonly component = HomeMenu;

    accordionSections: AccordionSection[] = [
        {
            title: 'Items Management',
            subtitle: 'managing items',
            details:
                'The Items management, where all the magic of managing items happens.\nBe an Items manager!',
            button: {
                text: 'Go To Items Management',
                action: () => NavigationService.goTo(Routes.ItemsManagement),
            },
        },
        {
            title: 'About Me',
            subtitle: 'Roi Dayan',
            details:
                'I am experienced in developing web and multi-platform mobile applications. when I develop a product, I focus on writing clean, readable and efficient code, both in terms of server-side and client-side, for the main goal, a product with a great user experience.',
            button: {
                text: 'Go To roidayan.net',
                action: () => NavigationService.goTo('https://roidayan.net/'),
                color: 'secondary',
            },
        },
        {
            title: 'About The Company',
            subtitle: 'pictures of peoples',
            details: <OurLeadership />,
        },
    ];

    connect(): HomeMenuProps {
        return {
            accordionSections: this.accordionSections,
        };
    }
}

const HomeMenuConnector = connectState(HomeMenuConnectorComponent, stores);
export default HomeMenuConnector;
