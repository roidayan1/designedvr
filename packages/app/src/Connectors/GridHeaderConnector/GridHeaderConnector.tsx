import BaseConnector, { connectState } from '../BaseConnector';
import BaseStore from '../../Services/StateService';
import GridHeader from '@dvr/components/src/GridHeader/GridHeader';
import { GridHeaderProps } from '@dvr/components/src/GridHeader/GridHeader.model';
import ModalService from '../../Services/ModalService';
import ItemsProvider from '../../Providers/Items/ItemsProvider';
import CreateNewItemModal from '@dvr/components/src/Modals/CreateNewItemModal/CreateNewItemModal';

const stores: Array<BaseStore<any>> = [];

export type GridHeaderConnectorProps = {};

class GridHeaderConnectorComponent extends BaseConnector<GridHeaderConnectorProps, GridHeaderProps> {
    readonly component = GridHeader;

    onSearch = async (value: string) => {
        ItemsProvider.setItemsFiltersSearch(value);
        await ItemsProvider.fetchSetGetItems();
    };

    handleCreateNewItem = () => {
        ModalService.show(CreateNewItemModal, {
            createNewItem: ItemsProvider.createNewItem,
        });
    };

    connect(): GridHeaderProps {
        return {
            searchValue: ItemsProvider.getItemsFilters().search,
            onSearch: this.onSearch,
            onCreateClick: this.handleCreateNewItem,
        };
    }
}

const GridHeaderConnector = connectState(GridHeaderConnectorComponent, stores);
export default GridHeaderConnector;
