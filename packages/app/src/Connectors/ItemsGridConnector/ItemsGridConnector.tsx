import BaseConnector, { connectState } from '../BaseConnector';
import BaseStore from '../../Services/StateService';
import ItemsGrid from '@dvr/components/src/ItemsGrid/ItemsGrid';
import { ItemsGridProps } from '@dvr/components/src/ItemsGrid/ItemsGrid.model';
import ItemsProvider from '../../Providers/Items/ItemsProvider';
import ModalService from '../../Services/ModalService';
import VerificationModal from '@dvr/components/src/Modals/VerificationModal/VerificationModal';

const stores: Array<BaseStore<any>> = [ItemsProvider.stores.ItemsStore];

export type ItemsGridConnectorProps = {};

class ItemsGridConnectorComponent extends BaseConnector<ItemsGridConnectorProps, ItemsGridProps> {
    readonly component = ItemsGrid;

    handleDeleteItem = (itemId: string) => {
        ModalService.show(VerificationModal, {
            title: 'Delete Item',
            message: 'Are you sure you want to delete this item ?',
            onYes: () => ItemsProvider.deleteItem(itemId),
        });
    };

    connect(): ItemsGridProps {
        return {
            fetchingMore: ItemsProvider.stores.ItemsStore.isLoading(),
            items: ItemsProvider.getItems(),
            onDeleteItem: this.handleDeleteItem,
        };
    }
}

const ItemsGridConnector = connectState(ItemsGridConnectorComponent, stores);
export default ItemsGridConnector;
