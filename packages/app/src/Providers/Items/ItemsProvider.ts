import { remove } from 'lodash';
import ToasterService from '../../Services/ToasterService';
import { Item, NewItem } from '@dvr/models/src/Items.model';
import ItemsStore from './Stores/ItemsStore';
import ItemsFiltersStore, { ItemsFiltersStoreValue } from './Stores/ItemsFiltersStore';
import GetItemsXhr from './Xhrs/GetItemsXhr';
import CreateItemXhr from './Xhrs/CreateItemXhr';
import DeleteItemXhr from './Xhrs/DeleteItemXhr';
import FilesProvider from '../files/FilesProvider';

class ItemsProvider {
    static stores = {
        ItemsStore,
        ItemsFiltersStore,
    };

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////// Items
    ///////////////////////////////////////////////////////////////////////////////////////
    static async fetchItems() {
        const itemsFilters = ItemsProvider.getItemsFilters();
        return await GetItemsXhr.request(itemsFilters).catch((error) =>
            ToasterService.addXHRError('Fetch Items', error)
        );
    }

    static async fetchSetGetItems() {
        ItemsProvider.stores.ItemsStore.setIsLoading(true);
        const response = await ItemsProvider.fetchItems();
        ItemsProvider.setItems(response || []);
        ItemsProvider.stores.ItemsStore.setIsLoading(false);
        return response;
    }

    static async createNewItem(newItem: NewItem) {
        if (newItem.imageFile) {
            const uploadResponse = await FilesProvider.uploadImages('item', [newItem.imageFile]).catch(
                (error) => ToasterService.addXHRError('Create New Item', error)
            );
            if (!uploadResponse?.[0]?.path)
                return ToasterService.addXHRError('Create New Item', 'Failed to upload image');
            newItem.imageUrl = uploadResponse[0].path;
        }
        const response = await CreateItemXhr.request({ newItem }).catch((error) =>
            ToasterService.addXHRError('Create New Item', error)
        );
        if (response) {
            ItemsProvider.fetchSetGetItems().then();
            ToasterService.addSuccess('The item was successfully created');
        }
    }

    static async deleteItem(itemId: string) {
        ItemsProvider.stores.ItemsStore.setIsLoading(true);
        const response = await DeleteItemXhr.request({ id: itemId }).catch((error) =>
            ToasterService.addXHRError('Delete Item', error)
        );
        if (response) {
            ItemsProvider.removeItem(itemId);
            ToasterService.addSuccess('The item was successfully deleted');
        }
        ItemsProvider.stores.ItemsStore.setIsLoading(false);
    }

    //////////////////////////////////
    static getItems() {
        return ItemsProvider.stores.ItemsStore.value();
    }

    static setItems(items: Array<Item>) {
        ItemsProvider.stores.ItemsStore.update(items);
    }

    static removeItem(itemId: string) {
        const items = ItemsProvider.getItems();
        remove(items, { id: itemId });
        ItemsProvider.setItems(items || []);
    }

    static clearItems() {
        return ItemsProvider.stores.ItemsStore.reset();
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    ///////// Items Filters
    ///////////////////////////////////////////////////////////////////////////////////////
    static getItemsFilters() {
        return ItemsProvider.stores.ItemsFiltersStore.value();
    }

    static setItemsFilters(itemsFilters: ItemsFiltersStoreValue) {
        ItemsProvider.stores.ItemsFiltersStore.update(itemsFilters);
    }

    static clearItemsFilters() {
        return ItemsProvider.stores.ItemsFiltersStore.reset();
    }

    static setItemsFiltersSearch(value: string) {
        const itemsFilters = ItemsProvider.getItemsFilters();
        itemsFilters.search = value;
        ItemsProvider.setItemsFilters(itemsFilters);
    }
}

export default ItemsProvider;
