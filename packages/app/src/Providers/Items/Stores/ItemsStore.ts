import BaseStore from '../../../Services/StateService';
import { Item } from '@dvr/models/src/Items.model';

class ItemsStore extends BaseStore<Array<Item>> {
    constructor() {
        super({
            saveToLocalStorage: false,
            saveToQuery: false,
            initialValue: [],
        });
    }
}

export default new ItemsStore();
