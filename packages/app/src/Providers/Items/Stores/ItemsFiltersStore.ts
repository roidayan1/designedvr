import BaseStore from '../../../Services/StateService';

export interface ItemsFiltersStoreValue {
    search?: string;
}

class ItemsFiltersStore extends BaseStore<ItemsFiltersStoreValue> {
    constructor() {
        super({
            saveToLocalStorage: false,
            saveToQuery: true,
            initialValue: {
                search: '',
            },
        });
    }
}

export default new ItemsFiltersStore();
