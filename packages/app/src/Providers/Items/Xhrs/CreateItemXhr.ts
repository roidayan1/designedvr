import BaseXhr, { XhrMethod } from '../../../Services/XhrService';
import { Item, NewItem } from '@dvr/models/src/Items.model';

interface Args {
    newItem: NewItem;
}

class CreateItemXhr extends BaseXhr<Item> {
    endpoint = '/items';
    method = XhrMethod.POST;

    request(args: Args): Promise<Item> {
        this.body = args.newItem || {};
        return super.request();
    }
}

export default new CreateItemXhr();
