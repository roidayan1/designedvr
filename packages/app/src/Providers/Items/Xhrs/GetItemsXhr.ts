import BaseXhr, { XhrMethod } from '../../../Services/XhrService';
import { Item } from '@dvr/models/src/Items.model';

interface Args {
    search?: string;
}

class GetItemsXhr extends BaseXhr<Array<Item>> {
    endpoint = '/items';
    method = XhrMethod.GET;

    request(args?: Args): Promise<Array<Item>> {
        this.query = {
            search: args?.search || '',
        };
        return super.request();
    }
}

export default new GetItemsXhr();
