import BaseXhr, { XhrMethod } from '../../../Services/XhrService';

interface Args {
    id: string;
}

class DeleteItemXhr extends BaseXhr<true> {
    endpoint = '/items/{id}';
    method = XhrMethod.DELETE;

    request(args: Args): Promise<true> {
        this.params = {
            id: args.id,
        };
        return super.request();
    }
}

export default new DeleteItemXhr();
