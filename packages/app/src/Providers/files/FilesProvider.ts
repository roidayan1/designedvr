import UploadImagesXhr from './Xhrs/UploadImagesXhr';

class FilesProvider {
    static async uploadImages(prefix: string, images: File[]) {
        return await UploadImagesXhr.request({ prefix: prefix, images });
    }
}

export default FilesProvider;
