import BaseXhr, { XhrMethod } from '../../../Services/XhrService';

interface Res {
    originalName: string;
    path: string;
}

interface Args {
    prefix: string;
    images: File[];
}

class UploadImagesXhr extends BaseXhr<Array<Res>> {
    endpoint = '/files/images';
    method = XhrMethod.POST;

    request(args: Args): Promise<Array<Res>> {
        this.body = {
            prefix: args.prefix,
        };
        this.files = args.images;
        return super.request();
    }
}

export default new UploadImagesXhr();
