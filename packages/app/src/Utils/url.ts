import { isObject } from 'lodash';
import NavigationService from '../Services/NavigationService';

export function joinUrl(...args: Array<string>) {
    return args.map((a) => a.replace(/^\/+|\/+$/g, '')).join('/');
}

export function addQueryParamsToUrl(url: string, query: { [key: string]: any }) {
    let urlObj = new URL(url);
    const queryChar = urlObj.search ? '&' : '?';
    const q = Object.entries(query)
        .map(([key, val]) => `${key}=${encodeURIComponent(val)}`)
        .join('&');
    return url + queryChar + q;
}

export function applyParamsToEndpoint(endpoint: string, params: { [key: string]: string }) {
    Object.keys(params).forEach((p) => {
        endpoint = endpoint.replace(`{${p}}`, params[p]);
    });
    return endpoint;
}

export function urlQueryStringToObj(str?: string) {
    const urlSearchParams = new URLSearchParams(str || NavigationService.history.location.search);
    const obj = {};
    for (const key of urlSearchParams.keys()) {
        const value = urlSearchParams.get(key);
        if (value === null) {
            obj[key] = null;
        } else if (value === undefined) {
            // do nothing
        } else {
            try {
                obj[key] = JSON.parse(value);
            } catch (err) {
                // obj[key] = value; => need to be commented to prevent manually data corruption
            }
        }
    }
    return obj;
}

export function objToUrlQueryString(obj?: { [key: string]: any }) {
    const urlSearchParams = new URLSearchParams();
    for (const key in obj) {
        let value: any = obj[key];
        if (value !== undefined && value !== null) {
            urlSearchParams.set(key, isObject(value) ? JSON.stringify(value) : value);
        }
    }
    urlSearchParams.sort();
    const search = urlSearchParams.toString();
    return search.length ? '?' + search : '';
}
