export interface NewItem {
    name: string;
    description: string;
    price: number;
    imageUrl?: string;
    imageFile?: File;
}

export interface Item {
    id: string;
    name: string;
    description: string;
    price: number;
    imageUrl: string;
    createdAt: Date | string;
    updatedAt: Date | string;
}
