import { TypeOrmModuleOptions } from '@nestjs/typeorm/dist/interfaces/typeorm-options.interface';
import configOrm from '../ormconfig.json';

require('dotenv').config();

const { SERVER_PORT, NODE_ENV } = process.env;

class Config {
    static port: number = parseInt(SERVER_PORT || '3005', 10);
    static isProd = NODE_ENV === 'production';
    static validation = {
        disableErrorMessages: false,
        whitelist: false,
        transform: true,
    };
    static dbOptions = {
        ...configOrm,
        host: Config.isProd ? 'database' : configOrm.host,
    } as TypeOrmModuleOptions & {
        host: string;
        port: number;
        username: string;
        password: string;
    };
}

export default Config;
