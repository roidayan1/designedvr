import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import path from 'path';
import Config from './config';

import { ConnectionsModule } from './connections/connections.module';
import { DefaultModule } from './modules/default/default.module';
import { FilesModule } from './modules/files/files.module';
import { ItemsModule } from './modules/items/items.module';

@Module({
    imports: [
        ServeStaticModule.forRoot({
            rootPath: path.resolve('../app/build'),
        }),
        TypeOrmModule.forRoot(Config.dbOptions),
        ConnectionsModule,
        FilesModule,
        //////////// Data Modules ////////////
        ItemsModule,
        //////////////////////////////////////
        DefaultModule, // -> should always be the last module in imports
    ],
})
export class AppModule {}
