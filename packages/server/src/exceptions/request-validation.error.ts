import { get } from 'lodash';
import { BadRequestException, HttpStatus } from '@nestjs/common';
import { BaseError } from './base.error';

export class RequestValidationError extends BaseError {
    check(exception: any): boolean {
        return (
            exception instanceof BadRequestException &&
            get(exception, 'stack', '').includes('ValidationPipe') &&
            get(exception, 'response.message.0')
        );
    }

    handle(exception: any) {
        const statusCode = get(exception, 'response.statusCode') || HttpStatus.BAD_REQUEST;

        const message = `Bad Request: ${get(exception, 'response.message.0')}.`;

        const source = this.constructor.name;

        const data = get(exception, 'response.message');

        const request = undefined;

        return { statusCode, message, source, data, request };
    }
}
