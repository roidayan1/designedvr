import { ArgumentsHost, Catch, ExceptionFilter, HttpStatus } from '@nestjs/common';
import { get, unset } from 'lodash';
import { Logger } from '../utils/logger';

import { BaseError } from './base.error';
import { RequestValidationError } from './request-validation.error';

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
    private logger: Logger = new Logger(this);

    private handlers: BaseError[] = [new RequestValidationError()];

    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();

        for (const handler of this.handlers) {
            if (handler.check(exception)) {
                const res = handler.handle(exception);
                ['request.headers.User-Agent'].forEach((p) => unset(res, p));
                this.logger.error(res.message, res, handler.constructor.name);
                return response.status(res.statusCode).json(res);
            }
        }

        const statusCode =
            get(exception, 'status') ||
            get(exception, 'response.statusCode') ||
            get(exception, 'response.status') ||
            HttpStatus.INTERNAL_SERVER_ERROR;

        const message =
            get(exception, 'message') ||
            get(exception, 'response.data.errorMessage') ||
            get(exception, 'response.data.errorCode') ||
            'Internal server error';

        const source = this.constructor.name;

        const data = get(exception, 'response.data');

        const request = undefined;

        this.logger.error('Exception was thrown', {
            statusCode,
            message,
            data,
        });

        return response.status(statusCode).json({ statusCode, message, source, data, request });
    }
}
