export abstract class BaseError {
    abstract check(exception: any): boolean;

    abstract handle(exception: any): {
        statusCode: number;
        message: string;
        source: string;
        data: any;
        request?: any;
    };
}
