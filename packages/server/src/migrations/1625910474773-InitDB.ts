import { MigrationInterface, QueryRunner, Table } from 'typeorm';

const items = [
    {
        name: '37a Sea&I Private Studio in Distinctive Boutique',
        description:
            'This well-appointed private studio located in the heart of Deerfield Beach provides everything needed for a restful and relaxing stay. Approximately one mile away from beautiful Deerfield Beach and half that distance to the Intracoastal Waterway; it’s an easy walk to numerous restaurants, shops, a fitness center and grocery stores. A 10-minute drive on US 1 N will put you in the center of Boca Raton at picturesque Mizner Park. Uber and Lyft are always available.',
        price: 75,
        imageUrl: 'https://guesty-listing-images.s3.amazonaws.com/production/large_13969407_241457828.jpg',
    },
    {
        name: '40 Sea&I Private Room in Distinctive Boutique',
        description:
            'This well-appointed private room located in the heart of Deerfield Beach provides everything needed for a restful and relaxing stay. Approximately one mile away from beautiful Deerfield Beach and half that distance to the Intracoastal Waterway; it’s an easy walk to numerous restaurants, shops, a fitness center and grocery stores. A 10-minute drive on US 1 N will put you in the center of Boca Raton at picturesque Mizner Park. Uber and Lyft are always available.',
        price: 60,
        imageUrl:
            'https://res.cloudinary.com/guesty/image/upload/w_1200,f_auto,fl_lossy/v1617162061/production/5a4e29a704976a1500f7f742/p1slovhf7fmckgqqrkis.jpg',
    },
    {
        name: 'New Reno Beach Front Studio Apt/w Parking',
        description:
            'This stunning oceanfront unit is the hidden gem of Hollywood beach. Lots of light, fully renovated, with all new furniture and décor with a tropical charm. Enjoy the beachfront courtyard directly on the boardwalk. North end boardwalk is quiet - but walking distance to restaurants, nightlife, Margaritaville resort is 5 minutes away.',
        price: 90,
        imageUrl: 'https://cdn.filepicker.io/api/file/dN0O9WPRFySafCiuerYQ',
    },
    {
        name: '*Newly Renovated* 3BR Home w/Hot tub',
        description:
            'This updated, newly furnished three bedroom home makes for the perfect getaway with all the comforts of home. Located in the perfect area as it nestled in the heart of Ft. Lauderdale, just 5 minutes to the amazing new Hard Rock hotel and casino. Tons of charm, new beds and comfy bedding makes this a perfect escape. New furnishings throughout with beautiful accents in tones of blue and aqua for a modern feel. Outdoor seating area has plenty of green space, patio chairs, propane grill, and an outdoor hot tub so you can truly sit back and relax.',
        price: 200,
        imageUrl: 'https://cdn.filepicker.io/api/file/yuHwNxZwSMaLneOXukzM',
    },
    {
        name: '*Private Heated Pool* /Beautiful 4BR Home 2BTH 𝘉𝘺 𝘋𝘦𝘴𝘪𝘨𝘯𝘦𝘥𝘝𝘙',
        description:
            'This beautifully updated, newly furnished 4 bedroom 2 bath home makes for the perfect getaway with all the comforts of home. Located in the perfect area as it nestled in the heart of Dania Beach. Tons of charm, new beds and comfy bedding makes this a perfect escape. New furnishings throughout with beautiful accents in tones of neutrals, blue and aqua for a modern feel. Outdoor seating area has, patio chairs, propane grill. Sparkling heated swimming pool with sun-shelf.',
        price: 300,
        imageUrl:
            'https://res.cloudinary.com/guesty/image/upload/w_1200,f_auto,fl_lossy/v1613064870/production/5a4e29a704976a1500f7f742/trac1gwcdsjwndrwtvn1.jpg',
    },
    {
        name: '*Stunning Views- 3 BR Waterfront Home* 𝘉𝘺 𝘋𝘦𝘴𝘪𝘨𝘯𝘦𝘥𝘝𝘙',
        description:
            'Beautiful water front home with spectacular views. New furnishings throughout with new beds and comfy bedding. Tons of natural light and gorgeous outdoor space and heated pool.',
        price: 300,
        imageUrl:
            'https://res.cloudinary.com/guesty/image/upload/w_1200,f_auto,fl_lossy/v1618413456/production/5a4e29a704976a1500f7f742/vygbqgyiwf0b89cqogqn.jpg',
    },
];

export class InitDB1625910474773 implements MigrationInterface {
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(
            new Table({
                name: 'items',
                columns: [
                    {
                        name: 'id',
                        type: 'varchar',
                        isPrimary: true,
                    },
                    {
                        name: 'name',
                        type: 'varchar',
                    },
                    {
                        name: 'description',
                        type: 'text',
                    },
                    {
                        name: 'price',
                        type: 'int',
                    },
                    {
                        name: 'imageUrl',
                        type: 'varchar',
                    },
                    {
                        name: 'createdAt',
                        type: 'timestamp',
                        default: 'now()',
                    },
                    {
                        name: 'updatedAt',
                        type: 'timestamp',
                        default: 'now()',
                    },
                ],
            }),
            true
        );
        await queryRunner.manager.createQueryBuilder().insert().into('items').values(items).execute();
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('items');
    }
}
