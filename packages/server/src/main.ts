import cookieParser from 'cookie-parser';
import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
import { json, urlencoded } from 'body-parser';
import { Logger, LoggingInterceptor } from './utils/logger';
import Config from './config';
import { createMySqlDatabase } from './utils/createMySqlDatabase';

import { AppModule } from './app.module';

import { AllExceptionsFilter } from './exceptions/all-exceptions.filter';

async function bootstrap() {
    const logger = new Logger('Main');

    if (!Config.isProd) {
        await createMySqlDatabase(Config.dbOptions.database as string);
    }

    const app = await NestFactory.create(AppModule);

    app.setGlobalPrefix('api');
    app.use(json({ limit: '10mb' }));
    app.use(urlencoded({ limit: '10mb', extended: true }));
    app.use(cookieParser());
    app.useGlobalFilters(new AllExceptionsFilter());
    app.useGlobalInterceptors(new LoggingInterceptor());
    app.useGlobalPipes(
        new ValidationPipe({
            disableErrorMessages: Config.validation.disableErrorMessages,
            whitelist: Config.validation.whitelist,
            transform: Config.validation.transform,
        })
    );

    const port = Config.port || 3005;
    await app.listen(port);

    logger.info(`Set global prefix to: "api"`);
    logger.info(`Server running on port: ${port}`);
}

bootstrap();
