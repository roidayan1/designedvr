import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

enum Color {
    Black = '\x1b[30m',
    Red = '\x1b[31m',
    Green = '\x1b[32m',
    Yellow = '\x1b[33m',
    Blue = '\x1b[34m',
    Magenta = '\x1b[35m',
    Cyan = '\x1b[36m',
    White = '\x1b[37m',
}

enum LogType {
    info = 'INFO',
    warn = 'WARNING',
    error = 'ERROR',
}

export class Logger {
    private readonly sender?: string;

    constructor(sender: string | { constructor: { name: string } }) {
        this.sender = this.getSenderName(sender);
    }

    private getSenderName = (sender?: string | { constructor: { name: string } }): string | undefined => {
        if (!sender) return undefined;
        let name: string;
        if (typeof sender === 'string') {
            name = sender;
        } else if (sender.constructor?.name) {
            name = sender.constructor.name;
        } else {
            name = '<UNKNOWN>';
        }
        return name;
    };

    private wrapWithColor = (text: string, color: Color): string => {
        return `${color}${text}\x1b[0m`;
    };

    private logMessage(prefix: string, message: string, data?: any, sender?: string) {
        let log = '';

        if (prefix) {
            log += `${prefix}:  `;
        }

        log += `${this.wrapWithColor(sender || this.sender || '', Color.Green)} - ${message}`;

        console.log(log);

        if (data !== undefined) {
            console.log(JSON.stringify(data, null, 4));
        }
    }

    public info(message: string, data?: any, sender?: string | { constructor: { name: string } }) {
        const prefix = this.wrapWithColor(LogType.info, Color.Blue);
        this.logMessage(prefix, message, data, this.getSenderName(sender));
    }

    public warn(message: string, data?: any, sender?: string | { constructor: { name: string } }) {
        const prefix = this.wrapWithColor(LogType.warn, Color.Yellow);
        this.logMessage(prefix, message, data, this.getSenderName(sender));
    }

    public error(message: string, data?: any, sender?: string | { constructor: { name: string } }) {
        const prefix = this.wrapWithColor(LogType.error, Color.Red);
        this.logMessage(prefix, message, data, this.getSenderName(sender));
    }
}

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
    logger = new Logger('RouterLogger');

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const ctx = context.switchToHttp();
        const request = ctx.getRequest();
        // const response = ctx.getResponse();

        const now = Date.now();
        this.logger.info(`Request ${now}: ${request.method} ${request.url}`);
        return next
            .handle()
            .pipe(tap(() => this.logger.info(`Request ${now}: finished after ${Date.now() - now}ms`)));
    }
}
