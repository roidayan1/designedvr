import mysql from 'mysql2';
import Config from '../config';

export function createMySqlDatabase(
    dbName: string,
    ifNotExists: boolean = true,
    charset: string = 'utf8mb4',
    collation: string = 'utf8mb4_unicode_ci'
): Promise<void> {
    return new Promise((resolve) => {
        const connection = mysql.createConnection({
            port: Config.dbOptions.port,
            host: Config.dbOptions.host,
            user: Config.dbOptions.username,
            password: Config.dbOptions.password,
        });
        connection.connect((error) => {
            if (error) throw error;
            connection.query(
                `CREATE DATABASE${
                    ifNotExists ? ' IF NOT EXISTS' : ''
                } ${dbName} CHARACTER SET ${charset} COLLATE ${collation}`,
                (err) => {
                    if (err) throw err;
                    resolve();
                }
            );
        });
    });
}
