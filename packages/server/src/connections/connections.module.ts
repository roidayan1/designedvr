import { Global, Module } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';

const providers: Provider[] = [];

@Global()
@Module({
    providers: providers,
    exports: providers,
})
export class ConnectionsModule {}
