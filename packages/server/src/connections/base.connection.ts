import * as _ from 'lodash';
import { HttpService, Inject, Injectable } from '@nestjs/common';
import { AxiosResponse, Method } from 'axios';
import { REQUEST } from '@nestjs/core';
import { Request } from 'express';
import { validateOrReject } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { ClassType } from 'class-transformer/ClassTransformer';
import { Logger } from '../utils/logger';

export type RequestConfig = {
    query?: { [key: string]: any };
    data?: { [key: string]: any };
    headers?: { [key: string]: string | string[] | undefined };
    timeout?: number;
    method?: Method;
};

@Injectable()
export class BaseConnection {
    private readonly logger = new Logger(this);

    protected readonly httpService: HttpService = new HttpService();
    protected baseUrl: string = '';
    protected headers: { [key: string]: string | string[] } = {};
    protected headersFromClient: string[] = [];
    @Inject(REQUEST) private readonly clientRequest!: Request;

    get = async (endpoint: string, config?: RequestConfig, validationClass?: ClassType<unknown>) => {
        return await this.request(endpoint, 'GET', config, validationClass);
    };

    post = async (endpoint: string, config?: RequestConfig, validationClass?: ClassType<unknown>) => {
        return await this.request(endpoint, 'POST', config, validationClass);
    };

    delete = async (endpoint: string, config?: RequestConfig, validationClass?: ClassType<unknown>) => {
        return await this.request(endpoint, 'DELETE', config, validationClass);
    };

    patch = async (endpoint: string, config?: RequestConfig, validationClass?: ClassType<unknown>) => {
        return await this.request(endpoint, 'PATCH', config, validationClass);
    };

    put = async (endpoint: string, config?: RequestConfig, validationClass?: ClassType<unknown>) => {
        return await this.request(endpoint, 'PUT', config, validationClass);
    };

    request = async (
        endpoint: string,
        method: Method,
        config: RequestConfig = {},
        validationClass?: ClassType<unknown>
    ): Promise<AxiosResponse> => {
        let url = /^https?:\/\//i.test(endpoint)
            ? endpoint
            : this.baseUrl.replace(/\/+$/, '') + '/' + endpoint.replace(/^\/+/, '');

        config.headers = {
            ...this.headers,
            ..._.pick(this.clientRequest.headers, this.headersFromClient),
            ...config.headers,
        };
        config.timeout = config.timeout ? config.timeout : 60000;
        config.method = method;

        const now = Date.now();
        this.logger.info(`Network request ${now} to: ${url}`, config);

        if (config.query?.constructor) {
            url += '?' + new URLSearchParams(config.query);
        }

        delete config.query;

        let response: AxiosResponse | undefined = undefined;
        try {
            response = await this.httpService.request({ ...{ url, method }, ...config }).toPromise();
            this.logger.info(`Network request ${now} finished after ${Date.now() - now}ms`);

            if (validationClass) {
                response.data = plainToClass(validationClass, response.data);
                response.data && (await validateOrReject(response.data));
            }

            return response;
        } catch (e) {
            this.logger.info(`Network request ${now} finished with error after ${Date.now() - now}ms`);
            if (!e.response) e.response = response;
            e.source = this.constructor.name;
            throw e;
        }
    };
}
