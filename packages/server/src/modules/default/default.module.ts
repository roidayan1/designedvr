import { Global, Module } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { DefaultController } from './default.controller';

const providers: Provider[] = [];

@Global()
@Module({
    controllers: [DefaultController],
    providers: providers,
    exports: providers,
})
export class DefaultModule {}
