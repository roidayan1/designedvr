import { All, Controller, HttpException, HttpStatus } from '@nestjs/common';

@Controller()
export class DefaultController {
    @All('*')
    async notFound() {
        throw new HttpException("Route doesn't exists", HttpStatus.NOT_FOUND);
    }
}
