import { Injectable } from '@nestjs/common';
import { Like, Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ItemEntity } from './items.entity';
import { Item, NewItem } from '@dvr/models/src/Items.model';
import { FindManyOptions } from 'typeorm/find-options/FindManyOptions';

@Injectable()
export class ItemsService {
    constructor(
        @InjectRepository(ItemEntity)
        private readonly itemsRepository: Repository<ItemEntity>
    ) {}

    async getItems(search: string = '', page: number = 1, limit: number = 0): Promise<Item[]> {
        const options: FindManyOptions = {};
        if (limit) options.take = limit;
        if (page) options.skip = (page - 1) * (limit || 20);
        if (search) options.where = [{ name: Like(`%${search}%`) }, { description: Like(`%${search}%`) }];

        return this.itemsRepository.find(options);
    }

    async createItem(newItem: NewItem): Promise<Item> {
        return this.itemsRepository.save(newItem);
    }

    async getItem(id?: string): Promise<Item | undefined> {
        return this.itemsRepository.findOne(id);
    }

    async updateItem(id: string, item: Partial<Item>): Promise<Item> {
        return this.itemsRepository.save({ ...item, id });
    }

    async removeItem(id: string): Promise<void> {
        await this.itemsRepository.delete(id);
    }
}
