import { Global, Module } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { ItemsService } from './items.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ItemEntity } from './items.entity';
import { ItemsController } from './items.controller';

const providers: Provider[] = [ItemsService];

@Global()
@Module({
    controllers: [ItemsController],
    providers: providers,
    exports: providers,
    imports: [TypeOrmModule.forFeature([ItemEntity])],
})
export class ItemsModule {}
