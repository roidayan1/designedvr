import { IsNumber, IsOptional, IsString, Min } from 'class-validator';
import { Exclude, Expose } from 'class-transformer';

@Exclude()
export class GetItemsRequestQuery {
    @Expose()
    @IsOptional()
    @IsString()
    search?: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(1)
    page?: number;

    @Expose()
    @IsOptional()
    @IsNumber()
    @Min(1)
    limit?: number;
}

@Exclude()
export class CreateItemRequestBody {
    @Expose()
    @IsString()
    name!: string;

    @Expose()
    @IsString()
    description!: string;

    @Expose()
    @IsNumber()
    price!: number;

    @Expose()
    @IsOptional()
    @IsString()
    imageUrl!: string;
}

export class GetItemRequestParams {
    @IsString()
    id!: string;
}

export class UpdateItemRequestParams {
    @IsString()
    id!: string;
}

@Exclude()
export class UpdateItemRequestBody {
    @Expose()
    @IsOptional()
    @IsString()
    name!: string;

    @Expose()
    @IsOptional()
    @IsString()
    description!: string;

    @Expose()
    @IsOptional()
    @IsNumber()
    price!: number;

    @Expose()
    @IsOptional()
    @IsString()
    imageUrl!: string;
}

export class RemoveItemRequestParams {
    @IsString()
    id!: string;
}
