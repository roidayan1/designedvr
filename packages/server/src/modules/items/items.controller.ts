import { Body, Controller, Delete, Get, Param, Post, Query } from '@nestjs/common';
import { ItemsService } from './items.service';
import {
    CreateItemRequestBody,
    GetItemRequestParams,
    GetItemsRequestQuery,
    RemoveItemRequestParams,
    UpdateItemRequestBody,
    UpdateItemRequestParams,
} from './items.model';

@Controller()
export class ItemsController {
    constructor(private readonly itemsService: ItemsService) {}

    @Get('items')
    async getItems(@Query() query: GetItemsRequestQuery) {
        return await this.itemsService.getItems(query.search, query.page, query.limit);
    }

    @Post('items')
    async createItem(@Body() body: CreateItemRequestBody) {
        return await this.itemsService.createItem(body);
    }

    @Get('items/:id')
    async getItem(@Param() params: GetItemRequestParams) {
        return await this.itemsService.getItem(params.id);
    }

    @Post('items/:id')
    async updateItem(@Param() params: UpdateItemRequestParams, @Body() body: UpdateItemRequestBody) {
        return this.itemsService.updateItem(params.id, body);
    }

    @Delete('items/:id')
    async removeItem(@Param() params: RemoveItemRequestParams) {
        return this.itemsService.removeItem(params.id);
    }
}
