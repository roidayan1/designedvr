import { Global, Module } from '@nestjs/common';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { FilesController } from './files.controller';
import { FilesService } from './files.service';

const providers: Provider[] = [FilesService];

@Global()
@Module({
    controllers: [FilesController],
    providers: providers,
    exports: providers,
})
export class FilesModule {}
