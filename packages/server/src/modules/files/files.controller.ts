import { Controller, Get, Param, Post, Res, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { Response } from 'express';
import { GetFileRequestParams } from './files.model';
import { editFileName, imageFileFilter } from './files.utils';

@Controller('files')
export class FilesController {
    @Post('images')
    @UseInterceptors(
        FilesInterceptor('files[]', 20, {
            storage: diskStorage({
                destination: './files',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        })
    )
    async uploadImages(@UploadedFiles() files: Express.Multer.File[]) {
        return files.map((file) => ({
            originalName: file.originalname,
            path: `/api/files/images/${file.filename}`,
        }));
    }

    @Get('images/:path')
    async getItem(@Res() res: Response, @Param() params: GetFileRequestParams) {
        return res.sendFile(`${params.path}`, { root: './files' });
    }
}
