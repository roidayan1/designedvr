import { IsString } from 'class-validator';

export class GetFileRequestParams {
    @IsString()
    path!: string;
}
