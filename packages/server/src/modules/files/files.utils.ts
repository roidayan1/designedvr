import { Request } from 'express';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';

export const editFileName = (
    req: Request,
    file: Express.Multer.File,
    callback: (error: Error | null, filename: string) => void
) => {
    const fileExtName = extname(file.originalname);
    const prefix = req.body.prefix ? req.body.prefix + '-' : '';
    callback(null, `${prefix}${uuidv4()}${fileExtName}`);
};

export const imageFileFilter = (
    _req: Request,
    file: {
        fieldname: string;
        originalname: string;
        encoding: string;
        mimetype: string;
        size: number;
        destination: string;
        filename: string;
        path: string;
        buffer: Buffer;
    },
    callback: (error: Error | null, acceptFile: boolean) => void
) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
        return callback(new Error('Only image files are allowed!'), false);
    }
    callback(null, true);
};
