import { ItemCard } from './ItemCard/ItemCard.model';

export type ItemsGridProps = {
    fetchingMore?: boolean;
    items?: ItemCard[];
    onDeleteItem?: (itemId: string) => void;
};

export interface ItemsGridState {}
