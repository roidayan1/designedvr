export interface ItemCard {
    id: string;
    name: string;
    description: string;
    price: number;
    imageUrl: string;
}

export type ItemCardProps = {
    item: ItemCard;
    onDeleteItem?: (itemId: string) => void;
};

export interface ItemCardState {}
