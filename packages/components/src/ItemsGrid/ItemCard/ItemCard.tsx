import React, { Component, ReactNode } from 'react';
import { ItemCardProps, ItemCardState } from './ItemCard.model';
import styles from './ItemCard.module.scss';
import { Card, CardActions, CardContent, IconButton, Typography } from '@material-ui/core';
import MediaPreview from '../../MediaPreview/MediaPreview';
import TextWithTooltip from '../../TextWithTooltip/TextWithTooltip';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';

class ItemCardComponent extends Component<ItemCardProps, ItemCardState> {
    state: ItemCardState = {};

    render(): ReactNode {
        const { item, onDeleteItem } = this.props;

        return (
            <Card className={styles.container}>
                <MediaPreview src={item.imageUrl} alt={item.name} className={styles.image} />
                <CardContent>
                    <Typography variant="h6" className={styles.text}>
                        {item.name}
                    </Typography>
                    <TextWithTooltip amountOfLines={2} tooltipContent={item.description}>
                        <Typography color="textSecondary" className={styles.text}>
                            {item.description}
                        </Typography>
                    </TextWithTooltip>
                </CardContent>
                <CardActions>
                    <Typography variant="h6" className={styles.price}>
                        {item.price}$
                    </Typography>
                    <IconButton onClick={() => onDeleteItem?.(item.id)}>
                        <DeleteForeverIcon color="secondary" />
                    </IconButton>
                </CardActions>
            </Card>
        );
    }
}

const ItemCard = ItemCardComponent;
export default ItemCard;
