import React, { Component, ReactNode } from 'react';
import { ItemsGridProps, ItemsGridState } from './ItemsGrid.model';
import styles from './ItemsGrid.module.scss';
import { LinearProgress, Typography } from '@material-ui/core';
import ItemCard from './ItemCard/ItemCard';

class ItemsGridComponent extends Component<ItemsGridProps, ItemsGridState> {
    state: ItemsGridState = {};

    render(): ReactNode {
        const { fetchingMore, items, onDeleteItem } = this.props;

        return (
            <div className={styles.container}>
                <div className={styles.cardsContainer}>
                    {items?.map((item, index) => (
                        <ItemCard key={index} item={item} onDeleteItem={onDeleteItem} />
                    ))}
                </div>
                {fetchingMore && <LinearProgress />}
                {!fetchingMore && !items?.length && (
                    <div className={styles.noResults}>
                        <Typography variant="h6" color="secondary">
                            No Results
                        </Typography>
                    </div>
                )}
            </div>
        );
    }
}

const ItemsGrid = ItemsGridComponent;
export default ItemsGrid;
