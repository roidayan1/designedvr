export interface EmployeeCard {
    name: string;
    position?: string;
    imageSource: string;
}

export type OurLeadershipProps = {};

export interface OurLeadershipState {}
