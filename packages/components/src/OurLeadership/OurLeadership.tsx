import React, { Component, ReactNode } from 'react';
import { EmployeeCard, OurLeadershipProps, OurLeadershipState } from './OurLeadership.model';
import styles from './OurLeadership.module.scss';
import { Card, CardContent, CardMedia, Typography } from '@material-ui/core';

import ShaharGoldboimImage from '@dvr/assets/images/ShaharGoldboim.jpg';
import AlonGoldboimImage from '@dvr/assets/images/AlonGoldboim.jpg';
import KathrinGoldboimImage from '@dvr/assets/images/KathrinGoldboim.jpg';

class OurLeadershipComponent extends Component<OurLeadershipProps, OurLeadershipState> {
    state: OurLeadershipState = {};

    employeesCards: EmployeeCard[] = [
        {
            name: 'Shahar Goldboim',
            position: 'CEO',
            imageSource: ShaharGoldboimImage,
        },
        {
            name: 'Alon Goldboim',
            position: 'CTO',
            imageSource: AlonGoldboimImage,
        },
        {
            name: 'Kathrin Goldboim',
            position: 'COO',
            imageSource: KathrinGoldboimImage,
        },
    ];

    render(): ReactNode {
        // const {} = this.props;
        // const {} = this.state;

        return (
            <div className={styles.container}>
                <Typography variant="h4" color="textPrimary" className={styles.text}>
                    Our Leadership
                </Typography>

                <div className={styles.cardsContainer}>
                    {this.employeesCards.map((employeeCard, index) => (
                        <Card key={index} className={styles.card}>
                            <CardMedia className={styles.cardImage} image={employeeCard.imageSource} />
                            <CardContent>
                                <Typography variant="h6" className={styles.text}>
                                    {employeeCard.name}
                                </Typography>
                                <Typography className={styles.text}>{employeeCard.position}</Typography>
                            </CardContent>
                        </Card>
                    ))}
                </div>
            </div>
        );
    }
}

const OurLeadership = OurLeadershipComponent;
export default OurLeadership;
