export type GridHeaderProps = {
    searchValue?: string;
    onSearch?: (value: string) => void;
    onCreateClick?: () => void;
};

export interface GridHeaderState {}
