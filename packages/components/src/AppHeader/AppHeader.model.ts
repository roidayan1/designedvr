export type AppHeaderProps = {
    appName?: string;
    onHomeClick?: () => void;
};

export interface AppHeaderState {}
