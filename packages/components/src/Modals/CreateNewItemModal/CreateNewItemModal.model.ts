import { NewItem } from '@dvr/models/src/Items.model';

export interface CreateNewItemModalStateError {
    name?: string;
    description?: string;
    price?: string;
    imageUrl?: string;
}

export type CreateNewItemModalProps = {
    createNewItem?: (newItem: NewItem) => void;
};

export interface CreateNewItemModalState {
    files: File[];
    imageUrl: string;
    isImageUrlIsFocused: boolean;
    errors: CreateNewItemModalStateError;
}
