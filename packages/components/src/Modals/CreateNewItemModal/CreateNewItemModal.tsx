import React, { ChangeEvent, Component, createRef, ReactNode } from 'react';
import styles from './CreateNewItemModal.module.scss';
import {
    CreateNewItemModalProps,
    CreateNewItemModalState,
    CreateNewItemModalStateError,
} from './CreateNewItemModal.model';
import { TextField, Tooltip } from '@material-ui/core';
import FileInput from '../../FileInput/FileInput';
import BaseModal from '../BaseModal';
import { BaseModalButton } from '../BaseModal.model';
import { isEmpty } from 'lodash';
import MediaPreview from '../../MediaPreview/MediaPreview';
import ImageIcon from '@material-ui/icons/Image';

class CreateNewItemModalComponent extends Component<CreateNewItemModalProps, CreateNewItemModalState> {
    // its better to use some validation library
    isValidForm: boolean = false;
    validateForm = () => {
        const errors: CreateNewItemModalStateError = {};

        const name = this.nameTF.current?.value || '';
        if (!name) errors.name = 'Name is required';
        else if (name.length < 10) errors.name = 'Name must be at least 10 characters';

        const description = this.descriptionTF.current?.value || '';
        if (!description) errors.description = 'Description is required';
        else if (description.length < 50) errors.description = 'Description must be at least 50 characters';

        const price = Number(this.priceTF.current?.value || 0);
        if (!price) errors.price = 'Price is required';
        else if (price < 0) errors.price = 'Price must be greater than 0';

        const imageUrl = this.state.imageUrl || '';
        if (this.state.files.length) {
        } else if (!imageUrl) errors.imageUrl = 'Image url is required if no image was attached';
        else if (!/^https:\/\//i.test(imageUrl)) errors.imageUrl = 'Image url must start with https://';

        this.setState({ errors });
        this.isValidForm = isEmpty(errors);
    };

    onCreate = () => {
        this.validateForm();
        if (this.isValidForm) {
            this.props.createNewItem?.({
                name: this.nameTF.current?.value || '',
                description: this.descriptionTF.current?.value || '',
                price: Number(this.priceTF.current?.value),
                imageUrl: this.state.imageUrl || '',
                imageFile: this.state.files?.[0],
            });
        }
    };

    rightButtons: BaseModalButton[] = [
        { label: 'Cancel' },
        {
            label: 'Create',
            onClick: this.onCreate,
            modalShouldStayOpen: () => !this.isValidForm,
        },
    ];

    nameTF = createRef<HTMLInputElement>();
    descriptionTF = createRef<HTMLInputElement>();
    priceTF = createRef<HTMLInputElement>();
    imageFile = createRef<HTMLInputElement>();

    state: CreateNewItemModalState = {
        files: [],
        imageUrl: '',
        isImageUrlIsFocused: false,
        errors: {},
    };

    onFilesChange = (files: File[]) => {
        this.setState({ files, imageUrl: '' });
    };

    onImageUrlChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        this.setState({ imageUrl: event.target.value });
    };
    onImageUrlFocus = () => this.setState({ isImageUrlIsFocused: true });
    onImageUrlBlur = () => this.setState({ isImageUrlIsFocused: false });

    render(): ReactNode {
        const { files, imageUrl, isImageUrlIsFocused, errors } = this.state;

        return (
            <BaseModal title={'Create New Item'} rightButtons={this.rightButtons} {...this.props}>
                <div className={styles.container}>
                    <TextField
                        inputRef={this.nameTF}
                        className={styles.input}
                        label="Name"
                        error={!!errors.name}
                        helperText={errors.name || ' '}
                    />
                    <TextField
                        inputRef={this.descriptionTF}
                        className={styles.input}
                        label="Description"
                        multiline
                        error={!!errors.description}
                        helperText={errors.description || ' '}
                    />
                    <TextField
                        inputRef={this.priceTF}
                        className={styles.input}
                        label="Price"
                        type="number"
                        InputProps={{
                            inputProps: {
                                min: 0,
                            },
                        }}
                        error={!!errors.price}
                        helperText={errors.price || ' '}
                    />
                    <div className={styles.imageUploadContainer}>
                        <FileInput
                            className={styles.fileInput}
                            inputRef={this.imageFile}
                            accept={'image/*'}
                            label={'Upload Image'}
                            onChange={this.onFilesChange}
                        />
                        {!files?.length && (
                            <TextField
                                value={imageUrl}
                                className={styles.inputImageUrl}
                                label={isImageUrlIsFocused || imageUrl ? 'Image Url' : 'Or Add an Image Url'}
                                onChange={this.onImageUrlChange}
                                onFocus={this.onImageUrlFocus}
                                onBlur={this.onImageUrlBlur}
                                error={!!errors.imageUrl}
                                helperText={errors.imageUrl || ' '}
                            />
                        )}
                        <Tooltip
                            className={styles.imagePreviewIcon}
                            interactive
                            arrow
                            title={
                                <MediaPreview
                                    src={imageUrl || (files?.[0] && URL.createObjectURL(files?.[0]))}
                                    alt="image preview"
                                    className={styles.imagePreview}
                                />
                            }>
                            <ImageIcon color="secondary" />
                        </Tooltip>
                    </div>
                </div>
            </BaseModal>
        );
    }
}

const CreateNewItemModal = CreateNewItemModalComponent;
export default CreateNewItemModal;
