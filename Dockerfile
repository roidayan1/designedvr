FROM node:14.17.0-alpine

# copy the files to the image
COPY . ./

# Install all dependencies
RUN yarn bootstrap

# build the app and the server
RUN yarn build

# Set start CMD
CMD ["yarn", "start:prod"]
